package net.seenaomi.n00bbook.web;

import java.io.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.seenaomi.n00bbook.beans.Post;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Servlet implementation class N00bbookServlet
 */
@WebServlet("/n00b")
public class N00bbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public N00bbookServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response); // always make your doGet call doPost
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String v = request.getParameter("start");
		try {
			if (v != null) {
				getPostEntries(request, response);
			}
			else {
				v = request.getParameter("name");
				if (v != null) {
					signPost(request, response);
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.put("error", "Something went wrong! Try again later!");
			PrintWriter writer = response.getWriter();
			writer.print(json.toString());
		}
	}

private void signPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Post entry = new Post();
	String v = request.getParameter("name");
	entry.setName(v);
	v = request.getParameter("status");
	entry.setStatus(v);
	v = request.getParameter("tags");
	entry.setTags(v);

	String [] text = {">:(", ":$", "|:-)", ":'(", ":)", ":D", ":-))", "XD", "8D", ":(", "%)", ":o", ":p", ";)"};
	String [] img = {"images/Angry.png", "images/Blushing.png", "images/Cool.png", "images/Crying.png", "images/Smile Face.png", "images/Smile.png", "images/Happy.png", "images/Laughing.png", "images/Nerd.png", "images/Sad.png", "images/Sick.png", "images/Surprise.png", "images/tongue.png", "images/Winking.png"};		
	
	while (true){
		boolean found=false;
		for (int i=0; i<text.length; i++) {
			String t = text[i];
			v = entry.getStatus(); 	
			int idx = v.indexOf(t);
			if (idx >= 0) {	
			entry.setStatus	(v.substring(0, idx) + "<img class=\"faces\" src=\"" +img[i] +"\"/>" + v.substring(idx+t.length()));
			found=true;
			}
		}
		if (!found) break;
	}
	try {
		entry.save();
		JSONObject json = new JSONObject();
		json.put("status", "Thanks for your Post!");
		PrintWriter writer = response.getWriter();
		writer.print(json.toString());
	}
	catch (Exception e) {
		e.printStackTrace();
		JSONObject json = new JSONObject();
		json.put("error", "Could not save Guestbook entry! Try again later!");
		PrintWriter writer = response.getWriter();
		writer.print(json.toString());
	}
}

private void getPostEntries(HttpServletRequest request, HttpServletResponse response) throws IOException {
	try {
		String v = request.getParameter("start");
		int start = Integer.parseInt(v); // this makes the string into an int
		System.out.println("Get start " +start);
		v = request.getParameter("count");
		int count = Integer.parseInt(v);
		List<Post> list = Post.load(start, count);
		// TODO do stuff with List like turn it into JSON
		JSONArray array = new JSONArray();
		System.out.println(array.toString());
		// []
		for (Post gb : list) { // for equals a loop gb is being
									// assigned to each n00bbook post once
									// there is no more posts then it stops
			JSONObject jo = new JSONObject();
			// {}
			jo.put("name", gb.getName());
			// {
			// "name": "Naomi See"
			// }
			jo.put("status", gb.getStatus());
			// {
			// "status": "Blah Blah words and feelings"
			// }
			jo.put("tags", gb.getTags());
			// {
			// "tags": "post, data, java, etc"
			// }
			// TODO rest of the fields go here
			array.put(jo);
			// [
			// {
			// "name": "Naomi See"
			// "status": "Blah Blah words and feelings"
			// "tags": "post, data, java, etc"
			// }
			// ]
		}
		String output = array.toString();
		PrintWriter writer = response.getWriter();
		writer.print(output);
	} catch (Throwable t) {
		t.printStackTrace();
	}
}
}
