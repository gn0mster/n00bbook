package net.seenaomi.n00bbook.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.naming.NamingException;

import net.seenaomi.sql.SQLUtil;

public class Post {
	private int id;
	private String name;
	private String status;
	private String tags;
	private String emoticon;
	private Date created; // import java.util.Date
	
	public static List<Post> load(int start, int count)
			throws SQLException, NamingException { // constructor method that
													// returns a list of many
													// n00bbook entries Source
													// > organize imports
													// list.util this is loading
													// from the DB
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Post> list = new LinkedList<Post>(); // LinkedList are
															// dynamically sized
															// (array list are
															// great if you need
															// to get something
															// random or
															// specific)
															// LinkedLists are
															// linear and always
															// start from the
															// beginning
		try {
			conn = SQLUtil.getConnection();
			ps = conn
					.prepareStatement("SELECT * from Post ORDER BY created desc LIMIT ? OFFSET ?");
			int n = 1;
			ps.setInt(n++, count);
			ps.setInt(n++, start);
			rs = ps.executeQuery();
			while (rs.next()) {
				Post gb = new Post();
				gb.setName(rs.getString("name"));
				gb.setStatus(rs.getString("status"));
				gb.setTags(rs.getString("tags"));
				list.add(gb);
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (SQLException e) {					
				}
			}
			if (ps != null) {
				try {
					ps.close();
				}
				catch (Exception e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Exception e) {
				}
			}
		}
		return list;
	}

	public void save() throws SQLException, NamingException { // saving this n00bbook entry that's filled in with
							// data to the DB
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = SQLUtil.getConnection();
			String sql = "INSERT INTO Post (name, status, tags) VALUES(?,?,?)";
			System.out.println("SQL: " + sql);
			ps = conn.prepareStatement(sql/*, PreparedStatement.RETURN_GENERATED_KEYS*/);
			int n = 1;
			ps.setString(n++, name);
			ps.setString(n++, status);
			ps.setString(n++, tags);
			ps.execute();
//			ResultSet rs = ps.getGeneratedKeys();
//			if (rs != null && rs.next()) {
//				id = rs.getInt(1);
//			}
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * @return the emoticon
	 */
	public String getEmoticon() {
		return emoticon;
	}
	/**
	 * @param emoticon the emoticon to set
	 */
	public void setEmoticon(String emoticon) {
		this.emoticon = emoticon;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}
	
}
