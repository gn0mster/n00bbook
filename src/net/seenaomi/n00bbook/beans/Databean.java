package net.seenaomi.n00bbook.beans;

import java.util.List;

public interface Databean<T> { //repeative methods can go into interfaces as a generic T = type
		public List<T> load(int start, int count);
		public void save();

	}
